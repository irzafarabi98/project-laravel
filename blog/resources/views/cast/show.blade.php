@extends('adminlte.master')

@section('content')

    <!-- Content Header (trage header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cast</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item active">Cast Index</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-trrimary">
                <div class="card-header">
                    <h3 class="card-title">List of Cast</h3>
                </div>
              <!-- /.card-header -->



              <div class="card-body">
                <table class="table table-bordered">
                    <tr><td>Nama</td><td> : </td><td>{{ $cast->nama }}</td></tr>
                    <tr><td>Umur</td><td> : </td><td>{{ $cast->umur }}</td></tr>
                    <tr><td>Bio</td><td> : </td><td>{{ $cast->bio }}</td></tr>
                </table>
              </div>

              
                 <!-- /.card-body -->
                <div class="card-footer">
                  <a href="/cast/create" class="btn btn-primary">Create</a>
                </div>
            </div>
            <!-- /.card -->
            
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
