<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('index');
});
Route::get('/master', function () {
    return view('adminlte.master');
});
Route::get('/table', function () {
    return view('adminlte.items.table');
});
Route::get('/data-table', function () {
    return view('adminlte.items.data-table');
});
Route::get('/register', 'RegisterController@form');
Route::get('/welcome', 'RegisterController@welcome');
// Route::post('/welcome', 'RegisterController@welcome_post');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{id}', 'CastController@show');
// Route::get('/cast/{id}/edit', 'CastController@edit');
// Route::put('/cast/{id}', 'CastController@update');
// Route::delete('/cast/{id}', 'CastController@destroy');

// Route::resource('casting', 'CastController');
Route::resource('cast', 'CastController');