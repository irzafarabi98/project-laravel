<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function index() {
        // $casts = DB::table('cast')->get(); // SELECT * FROM cast
        $casts = Cast::all(); // Eloquent ORM
        // dd($casts->all());
        return view('cast.index', compact('casts'));
    }
    public function create() {
        return view('cast.create');
    }
    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            "nama" => 'required|unique:cast|max:24',
            "umur" => 'required',
            "bio" => 'required'
        ]);
        // $query = DB::table('cast')->insert([
        //     "nama" => $request['nama'],
        //     "umur" => $request['umur'],
        //     "bio" => $request['bio']
        // ]);
        // $cast = new Cast;
        // $cast->nama = $request['nama'];
        // $cast->umur = $request['umur'];
        // $cast->bio = $request['bio'];
        // $cast->save(); // insert into cast
        $cast = Cast::create([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]); // mass assignment
        return redirect('/cast')->with('success', 'Cast created successfully!'); 
    }
    public function show($id) {
        // $cast = DB::table('cast')->where('id',$id)->first();
        $cast = Cast::find($id); // Eloquent ORM
        // dd($cast);
        return view('cast.show', compact('cast'));
    }
    public function edit($id) {
        $cast = DB::table('cast')->where('id',$id)->first();
        // dd($cast);
        return view('cast.edit', compact('cast'));
    }
    public function update($id, Request $request) {
        $request->validate([
            "nama" => 'required|max:24',
            "umur" => 'required',
            "bio" => 'required'
        ]);
        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         "nama" => $request['nama'],
        //         "umur" => $request['umur'],
        //         "bio" => $request['bio']
        // ]);
        $update = Cast::where('id', $id)->update([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]); // Eloquent ORM
        // dd($cast);
        return redirect('/cast')->with('success', 'Cast updated successfully!');
    }
    public function destroy($id) {
        // $query = DB::table('cast')->where('id', $id)->delete();
        Cast::destroy($id); // Eloquent ORM
        return redirect('/cast')->with('success', 'Cast deleted successfully!');
        // dd($cast);
    }
}
