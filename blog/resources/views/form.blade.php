<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" name="firstname">
        <p>Last name:</p>
        <input type="text" name="lastname">
        <p>Gender :</p>
        <input type="radio" name="gender" value="male"><label>Male</label><br>
        <input type="radio" name="gender" value="female"><label>Feale</label><br>
        <input type="radio" name="gender" value="other"><label>Other</label><br>
        <p>Nationality :</p>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Indonesia">Indonesia</option>
        </select>
        <p>Languange Spoken :</p>
        <input type="checkbox" id="bahasa" value="bahasa">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" value="other">
        <label for="other">Other</label><br>
        <p>Bio</p>
        <textarea name="bio" rows="6"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>