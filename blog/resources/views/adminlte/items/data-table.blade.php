@extends('adminlte.master')

@section('content')

@include('adminlte.partials.data-table')

@endsection

@push('scripts')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush